﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CzasReakcji
{
    public partial class Reaction : Form
    {
        Stopwatch timer;
        bool start = false;

        public Reaction()
        {
            InitializeComponent();
            button.Text = "START";
            button.BackColor = Color.Red;

        }

        public async Task ReactionTest()
        {
            if (start)
            {
                timer.Stop();
                TimeSpan timespan = timer.Elapsed;
                string timeString = String.Format("{0:00}:{1:00}:{2:000}", timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
                MessageBox.Show(timeString);
                button.BackColor = Color.Red;
                button.Text = "START";
                start = false;
            }
            else
            {
                button.BackColor = Color.Red;
                button.Text = "Nie naciskaj";
                Random rnd = new Random();
                int number = rnd.Next(1, 10000);
                button.Enabled = false;
                await Task.Delay(3000 + number);
                button.Enabled = true;
                button.BackColor = Color.Green;
                button.Text = "CLICK";
                start = true;
                timer = Stopwatch.StartNew();
            }
        }

        private async void button_Click(object sender, EventArgs e)
        {
            await ReactionTest();
        }
    }
}
